# Libraries GLPI plugin

## Подготовка к установке

Для корректной загрузки сторонних библиотек создайте файл `<GLPI_ROOT>/config/config_path.php` следующего содержания:

```
<?php

$libraries_composer = __DIR__ . '/../plugins/libraries/composer/vendor/autoload.php';
if (file_exists($libraries_composer)) {
   require_once $libraries_composer;
}
```

Хук `plugin_init_hook()` для подключения сторонних библиотек не годится, т.к. порядок инициализации модулей может измениться.

## Contributing

* Open a ticket for each bug/feature so it can be discussed
* Follow [development guidelines](http://glpi-developer-documentation.readthedocs.io/en/latest/plugins/index.html)
* Refer to [GitFlow](http://git-flow.readthedocs.io/) process for branching
* Work on a new branch on your own fork
* Open a PR that will be reviewed by a developer
