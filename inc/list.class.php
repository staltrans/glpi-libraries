<?php

/**
 -------------------------------------------------------------------------
 Libraries plugin for GLPI
 Copyright (C) 2018 by the Staltrans Development Team.

 https://bitbucket.org/staltrans/libraries
 -------------------------------------------------------------------------

 LICENSE

 This file is part of Libraries.

 Libraries is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 Libraries is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Libraries. If not, see <http://www.gnu.org/licenses/>.
 --------------------------------------------------------------------------
 */

class PluginLibrariesList extends CommonGLPI {

   const VIEW_LIST = 1024;

   public $taborientation = 'vertical';
   static $rightname = 'plugin_libraries_list';
   protected $displaylist = false;

   static function getTypeName($nb = 0) {
      return __('Библиотеки', 'libraries');
   }

   static function isNewID($ID) {
      return false;
   }

   static function canView() {
      return Session::haveRight(self::$rightname, self::VIEW_LIST);
   }

   static function getMenuName() {
      return __('Libraries', 'libraries');
   }

   static function getURL() {
      global $CFG_GLPI;
      return $CFG_GLPI['root_doc'] . '/plugins/libraries/front/list.php';
   }

   function getRights($interface = 'central') {
      return [
         self::VIEW_LIST => __('View'),
      ];
   }

   function defineTabs($options = []) {

      $ong = [];
      $this->addStandardTab(__CLASS__, $ong, $options);
      return $ong;
   }

   function getTabNameForItem(CommonGLPI $item, $withtemplate = 0) {
      if ($item->getType() == __CLASS__) {
         $composer = new PluginLibrariesComposer();
         $tabs = [1 => __('Libraries', 'libraries')];
         foreach ($composer->tabs() as $item) {
            $tabs[] = $item['title'];
         }
         return $tabs;
      }
      return '';
   }

   static function displayTabContentForItem(CommonGLPI $item, $tabnum = 1, $withtemplate = 0) {
      if ($item->getType() == __CLASS__) {
         $composer = new PluginLibrariesComposer();
         $tabs = [1 => ['content' => $composer->installed()]];
         $tabs = $tabs + $composer->tabs();
         foreach ($tabs as $key => $item) {
            if ($tabnum == $key) {
               echo $item['content'];
            }
         }
      }
      return true;
   }

}
