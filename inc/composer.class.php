<?php

/**
 -------------------------------------------------------------------------
 Libraries plugin for GLPI
 Copyright (C) 2018 by the Staltrans Development Team.

 https://bitbucket.org/staltrans/libraries
 -------------------------------------------------------------------------

 LICENSE

 This file is part of Libraries.

 Libraries is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 Libraries is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Libraries. If not, see <http://www.gnu.org/licenses/>.
 --------------------------------------------------------------------------
 */

class PluginLibrariesComposer {

   public $composer  = PLUGIN_LIBRARIES_COMPOSER . '/composer.json';
   public $autoload  = PLUGIN_LIBRARIES_COMPOSER . '/vendor/autoload.php';
   public $installed = PLUGIN_LIBRARIES_COMPOSER . '/vendor/composer/installed.json';

   public function installed() {
      if (file_exists($this->installed)) {
         $handle = fopen($this->installed, 'r');
         $content = fread($handle, filesize($this->installed));
         $list = json_decode($content, true);
         if (is_array($list)) {
            $output = '<div class="center"><table class="tab_cadrehov">';
            $output .= '<tr>';
            $output .= '<th colspan="3">Composer</th>';
            $output .= '</tr>';
            $output .= '<tr>';
            $output .= '<th>' . __('Name') . '</th>';
            $output .= '<th>' . __('Version') . '</th>';
            $output .= '<th>' . __('Description') . '</th>';
            $output .= '</tr>';
            foreach ($list as $item) {
               $output .= '<tr>';
               if (empty($item['homepage'])) {
                  $output .= '<td>' . $item['name'] . '</td>';
               } else {
                  $output .= '<td><a href="' . $item['homepage'] . '">' . $item['name'] . '</a></td>';
               }
               $output .= '<td>' . $item['version'] . '</td>';
               $output .= '<td>' . $item['description'] . '</td>';
               $output .= '</tr>';
            }
            $output .= '</table></div>';
            return $output;
         }
      }
      return '';
   }

   public function tabs() {
      $plug = new Plugin();
      $pluglist = $plug->find('', 'name, directory');
      $tabs = [];
      $key = 2;
      foreach ($pluglist as $item) {
         if ($item['state'] != Plugin::ACTIVATED) {
            $function = $this->helpHook($item['directory']);
            $this->load($item['directory']);
            if (function_exists($function)) {
               $vals = $function();
               if (isset($vals['title']) && isset($vals['content'])) {
                  $tabs[$key++] = $function();
               }
            }
         }
      }
      return $tabs;
   }

   protected function helpHook($plugin) {
      return "plugin_{$plugin}_libraries_composer_help";
   }

   protected function load($plugin) {
      if (file_exists(GLPI_ROOT . "/plugins/$plugin/setup.php")) {
         include_once(GLPI_ROOT . "/plugins/$plugin/setup.php");
      }
   }

}
